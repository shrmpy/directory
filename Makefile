
build:
	mkdir -p functions
	go get github.com/aws/aws-lambda-go/events
	go get github.com/aws/aws-lambda-go/lambda
	go get gitlab.com/shrmpy/twitch-sdk
	go get gitlab.com/joinery-labs/helical

	GOBIN=${PWD}/functions go install ./...

