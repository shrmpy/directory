package main

import (
	"encoding/json"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	sdk "gitlab.com/shrmpy/twitch-sdk"
)


type qtype struct {
	ChannelName string `json:"channelname"`
}

func handler(request events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	name, err := extractField(request.Body)
	if err != nil {
		return &events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       "JSON is missing the 'channelname' property",
		}, nil
	}

	//DEBUG DEBUG
	cli, tok := getEnvVars()
	// TODO send JSON request to obtain list
	answer := sdk.Users(cli, tok, name)
	////answer, _ := json.Marshal(payload)
	//DEBUG DEBUG


	headers := map[string]string{
		"Content-Type":  "application/json",
		////"Cache-Control": "public, max-age=300",
	}
	return &events.APIGatewayProxyResponse{
		StatusCode:      200,
		Headers:         headers,
		Body:            string(answer),
		IsBase64Encoded: false,
	}, nil
}

func main() {
	lambda.Start(handler)
}

// get environment variables
func getEnvVars() (c string, t string) {
	c = os.Getenv("API_CLIENT")
	t = os.Getenv("API_TOKEN")
	return
}

// grab a field value from the request body
func extractField(body string) (ch string, err error) {
	var qry qtype
	ch = ""
	// expect JSON that contains a "name" field
	err = json.Unmarshal([]byte(body), &qry)
	if err != nil {
		return
	}

	ch = qry.ChannelName
	return
}
