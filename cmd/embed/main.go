package main

import (
	"encoding/json"
	"fmt"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	sdk "gitlab.com/joinery-labs/helical"
)

var conf *sdk.Config

func init() {
	conf = sdk.NewConfig()
	conf.APIPartner(os.Getenv("API_CLIENT"))
	conf.APIToken(os.Getenv("API_TOKEN"))
	conf.EmbedParent(os.Getenv("EMBED_PARENT"))
}

func main() {
	lambda.Start(handler)
}

func handler(ev events.APIGatewayProxyRequest) (*events.APIGatewayProxyResponse, error) {
	name, err := channelName(ev.Body)
	if err != nil {
		return &events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       "JSON is missing the 'channelname' property",
		}, nil
	}

	//TODO regex to validate input 4-25 size a-zA-Z | underscore

	buff := sdk.Users(conf, name)
	var users UsersResponse
	if err := json.Unmarshal(buff, &users); err != nil {
		return &events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       "API JSON 'id' property",
		}, nil
	}
	// default to live stream
	vod := fmt.Sprintf("channel=%s", name)
	if len(users.Data) > 0 {
		body := sdk.Videos(conf, users.Data[0].Id)
		var videos VideosResponse
		if err := json.Unmarshal(body, &videos); err != nil {
			return &events.APIGatewayProxyResponse{
				StatusCode: 400,
				Body:       "API JSON VOD list",
			}, nil
		}
		if len(videos.Data) > 0 {
			vod = fmt.Sprintf("video=%s", videos.Data[0].Id)
		}
	}

	/*
		headers := map[string]string{
			"Content-Type":  "application/json",
			////"Cache-Control": "public, max-age=300",
		}*/
	return &events.APIGatewayProxyResponse{
		StatusCode: 200,
		////		Headers:         headers,
		Body:            sdk.Embed(conf, vod),
		IsBase64Encoded: false,
	}, nil
}

type qtype struct {
	ChannelName string `json:"channelname"`
}

// grab a field value from the request body
func channelName(body string) (ch string, err error) {
	var qry qtype
	ch = ""
	// expect JSON that contains a "name" field
	err = json.Unmarshal([]byte(body), &qry)
	if err != nil {
		return
	}

	ch = qry.ChannelName
	return
}

type UsersResponse struct {
	Data []User `json:"data"`
}

type VideosResponse struct {
	Data       []Video     `json:"data"`
	Pagination interface{} `json:"pagination"`
}

type User struct {
	Id    string `json:"id"`
	Login string `json:"login"`
}

type Video struct {
	Id       string `json:"id"`
	UserId   string `json:"user_id"`
	UserName string `json:"user_name"`
	Title    string `json:"title"`
	URL      string `json:"url"`

	CreatedAt string `json:"created_at"`
	Viewable  string `json:"viewable"`
	Type      string `json:"type"`
}
